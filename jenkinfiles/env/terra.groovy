pipeline {
agent any

 stages {
  stage ('navigate to main.tf path') {
   steps {
    sh 'cd terraform/env | pwd'
    sh 'ls'
   }
  }
  stage('Provision infrastructure') {
   steps {
    sh 'ls'
    sh 'sudo terraform init'
    sh 'sudo terraform plan'
    sh 'sudo terraform apply -auto-approve'
   }
  }

 /*stage('Destroy Infra') {
     
     steps {
         sh 'sudo terraform destroy -auto-approve'
     }
}*/
    
 }
}
